            <!--begin::Base Scripts -->
            <script src="assets/styles/vendors/base/vendors.bundle.js" type="text/javascript"></script>
            <script src="assets/styles/demo/demo7/base/scripts.bundle.js" type="text/javascript"></script>
            <!--end::Base Scripts -->
            <!--begin::Page Vendors Scripts -->
            <script src="assets/styles/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
            <!--end::Page Vendors Scripts -->
            <!--begin::Page Resources -->
            <!--end::Page Resources -->
            <script>
            $('.m-widget4__item').css('cursor', 'pointer');
            $(".m-widget4__item").on("click", function() {

                var avatar = $(this).find('img:first');
                var status = avatar.attr('active');
                var name = avatar.attr('alt');

                deactive(function() {
                    avatar.attr('active', (status == 'false' ? 'true' : 'false'));
                    avatar.attr('src', (LetterAvatar(name, 50, status == 'false' ? 'true' : 'false')));
                });

                $("#detail\\[avatar\\]").attr("src", LetterAvatar($(this).find('img:first').attr('alt'), 50, 'true'));
                $("#detail\\[name\\]").text($(this).find('img:first').attr('alt'));
                $("#detail\\[extension\\]").text($(this).find('img:first').attr('extension'));
                $("#detail\\[role\\]").text($(this).find('img:first').attr('role'));
                $("#detail\\[email\\]").text($(this).find('img:first').attr('email'));

                /* Check if detail view is visible */
                if (status == "false") {
                    $("#detail").show();
                } else {
                    $("#detail").hide();
                }
            });

            function deactive(callback) {
                Array.prototype.forEach.call(document.querySelectorAll('img.avatar'), function(all) {
                    all.setAttribute('active', 'false');
                    all.setAttribute('src', LetterAvatar(all.getAttribute('alt'), 50, 'false'));
                });
                callback();
            }
            </script>
            <script>
            (function(w, d) {

                function LetterAvatar(name, size, status) {
                    name = name || '';
                    size = size || 60;

                    /*var colours = [
                            "#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50",
                            "#f1c40f", "#e67e22", "#e74c3c", "#ecf0f1", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"
                        ],*/
                    var colours = ["#afafaf", "#ef7d00"],
                        nameSplit = String(name).toUpperCase().split(' '),
                        initials, charIndex, colourIndex, canvas, context, dataURI;


                    if (nameSplit.length == 1) {
                        initials = nameSplit[0] ? nameSplit[0].charAt(0) : '?';
                    } else {
                        initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
                    }

                    if (w.devicePixelRatio) {
                        size = (size * w.devicePixelRatio);
                    }

                    charIndex = (initials == '?' ? 72 : initials.charCodeAt(0)) - 64;
                    colourIndex = (status == "true" ? 2 : 1);
                    canvas = d.createElement('canvas');
                    canvas.width = size;
                    canvas.height = size;
                    context = canvas.getContext("2d");

                    context.fillStyle = colours[colourIndex - 1];
                    context.fillRect(0, 0, canvas.width, canvas.height);
                    context.font = Math.round(canvas.width / 2) + "px Arial";
                    context.textAlign = "center";
                    context.fillStyle = "#FFF";
                    context.fillText(initials, size / 2, size / 1.5);

                    dataURI = canvas.toDataURL();
                    canvas = null;

                    return dataURI;
                }

                LetterAvatar.transform = function() {

                    Array.prototype.forEach.call(d.querySelectorAll('img[avatar]'), function(img, name) {
                        name = img.getAttribute('avatar');
                        status = img.getAttribute('active');
                        img.src = LetterAvatar(name, img.getAttribute('width'), status);
                        img.removeAttribute('avatar');
                        img.setAttribute('alt', name);
                    });
                };


                // AMD support
                if (typeof define === 'function' && define.amd) {

                    define(function() { return LetterAvatar; });

                    // CommonJS and Node.js module support.
                } else if (typeof exports !== 'undefined') {

                    // Support Node.js specific `module.exports` (which can be a function)
                    if (typeof module != 'undefined' && module.exports) {
                        exports = module.exports = LetterAvatar;
                    }

                    // But always support CommonJS module 1.1.1 spec (`exports` cannot be a function)
                    exports.LetterAvatar = LetterAvatar;

                } else {

                    window.LetterAvatar = LetterAvatar;

                    d.addEventListener('DOMContentLoaded', function(event) {
                        LetterAvatar.transform();
                    });
                }

            })(window, document);
            </script>
</body>

</html>