<?php
	require 'header.php';
?>
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <!-- BEGIN: Header -->
            <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
                <div class="m-container m-container--fluid m-container--full-height">
                    <div class="m-stack m-stack--ver m-stack--desktop">
                        <!-- BEGIN: Brand -->
                        <div class="m-stack__item m-brand ">
                            <div class="m-stack m-stack--ver m-stack--general">
                                <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                    <a href="index.html" class="m-brand__logo-wrapper">
                                <img alt="" src="assets/images/logos/air-mobile.png" />
                            </a>
                                </div>
                                <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                    <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                    <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>
                                    <!-- END -->
                                    <!-- BEGIN: Responsive Header Menu Toggler -->
                                    <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>
                                    <!-- END -->
                                    <!-- BEGIN: Topbar Toggler -->
                                    <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>
                                    <!-- BEGIN: Topbar Toggler -->
                                </div>
                            </div>
                        </div>
                        <!-- END: Brand -->
                        <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                            <!-- BEGIN: Horizontal Menu -->
                            <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn">
                                <i class="la la-close"></i>
                            </button>
                            <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
                            </div>
                            <!-- END: Horizontal Menu -->
                            <!-- BEGIN: Topbar -->
                            <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                                <div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-light" id="m_quicksearch" m-quicksearch-mode="default">
                                    <!--BEGIN: Search Form -->
                                    <form class="m-header-search__form">
                                        <div class="m-header-search__wrapper">
                                            <span class="m-header-search__icon-search" id="m_quicksearch_search">
                                                <i class="flaticon-search"></i>
                                            </span>
                                            <span class="m-header-search__input-wrapper">
                                                <input autocomplete="off" type="text" name="q" class="m-header-search__input" value="" placeholder="Search..." id="m_quicksearch_input">
                                            </span>
                                            <span class="m-header-search__icon-close" id="m_quicksearch_close">
                                                <i class="la la-remove"></i>
                                            </span>
                                            <span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
                                                <i class="la la-remove"></i>
                                            </span>
                                        </div>
                                    </form>
                                    <!--END: Search Form -->
                                    <!--BEGIN: Search Results -->
                                    <div class="m-dropdown__wrapper">
                                        <div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
                                                    <div class="m-dropdown__content m-list-search m-list-search--skin-light">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--BEGIN: END Results -->
                                </div>
                                <div class="m-stack__item m-topbar__nav-wrapper">
                                    <ul class="m-topbar__nav m-nav m-nav--inline">
                                        <li class="m-nav__item m-topbar__notifications m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center  m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                            <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                                        <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
                                        <span class="m-nav__link-icon">
                                                    <span class="m-nav__link-icon-wrapper">
                                                        <i class="flaticon-alarm"></i>
                                                    </span>
                                                </span>
                                    </a>
                                            <div class="m-dropdown__wrapper">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__header m--align-center">
                                                        <span class="m-dropdown__header-title">9 New</span>
                                                        <span class="m-dropdown__header-subtitle">User Notifications</span>
                                                    </div>
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                                                                <li class="nav-item m-tabs__item">
                                                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
                                                                Alerts
                                                            </a>
                                                                </li>
                                                                <li class="nav-item m-tabs__item">
                                                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">Events</a>
                                                                </li>
                                                                <li class="nav-item m-tabs__item">
                                                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">Logs</a>
                                                                </li>
                                                            </ul>
                                                            <div class="tab-content">
                                                                <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                                                                    <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                                        <div class="m-list-timeline m-list-timeline--skin-light">
                                                                            <div class="m-list-timeline__items">
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                                                                    <span class="m-list-timeline__text">12 new users registered</span>
                                                                                    <span class="m-list-timeline__time">Just now</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge"></span>
                                                                                    <span class="m-list-timeline__text">System shutdown
                                                                                        <span class="m-badge m-badge--success m-badge--wide">pending</span>
                                                                                    </span>
                                                                                    <span class="m-list-timeline__time">14 mins</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge"></span>
                                                                                    <span class="m-list-timeline__text">New invoice received</span>
                                                                                    <span class="m-list-timeline__time">20 mins</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge"></span>
                                                                                    <span class="m-list-timeline__text">DB overloaded 80%
                                                                                        <span class="m-badge m-badge--info m-badge--wide">settled</span>
                                                                                    </span>
                                                                                    <span class="m-list-timeline__time">1 hr</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge"></span>
                                                                                    <span class="m-list-timeline__text">System error -
                                                                                        <a href="#" class="m-link">Check</a>
                                                                                    </span>
                                                                                    <span class="m-list-timeline__time">2 hrs</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item m-list-timeline__item--read">
                                                                                    <span class="m-list-timeline__badge"></span>
                                                                                    <span href="" class="m-list-timeline__text">New order received
                                                                                        <span class="m-badge m-badge--danger m-badge--wide">urgent</span>
                                                                                    </span>
                                                                                    <span class="m-list-timeline__time">7 hrs</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item m-list-timeline__item--read">
                                                                                    <span class="m-list-timeline__badge"></span>
                                                                                    <span class="m-list-timeline__text">Production server down</span>
                                                                                    <span class="m-list-timeline__time">3 hrs</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge"></span>
                                                                                    <span class="m-list-timeline__text">Production server up</span>
                                                                                    <span class="m-list-timeline__time">5 hrs</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                                                                    <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                                        <div class="m-list-timeline m-list-timeline--skin-light">
                                                                            <div class="m-list-timeline__items">
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                                    <a href="" class="m-list-timeline__text">New order received</a>
                                                                                    <span class="m-list-timeline__time">Just now</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
                                                                                    <a href="" class="m-list-timeline__text">New invoice received</a>
                                                                                    <span class="m-list-timeline__time">20 mins</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                                    <a href="" class="m-list-timeline__text">Production server up</a>
                                                                                    <span class="m-list-timeline__time">5 hrs</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                    <a href="" class="m-list-timeline__text">New order received</a>
                                                                                    <span class="m-list-timeline__time">7 hrs</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                    <a href="" class="m-list-timeline__text">System shutdown</a>
                                                                                    <span class="m-list-timeline__time">11 mins</span>
                                                                                </div>
                                                                                <div class="m-list-timeline__item">
                                                                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                    <a href="" class="m-list-timeline__text">Production server down</a>
                                                                                    <span class="m-list-timeline__time">3 hrs</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                                                                    <div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
                                                                        <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                                                            <span class="">All caught up!
                                                                                <br>No new logs.</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                            <a href="#" class="m-nav__link m-dropdown__toggle">
                                                <span class="m-topbar__userpic m--hide">
                                                    <img src="assets/images/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless m--img-centered" alt="" />
                                                </span>
                                        <span class="m-nav__link-icon m-topbar__usericon">
                                                    <span class="m-nav__link-icon-wrapper">
                                                        <i class="flaticon-user-ok"></i>
                                                    </span>
                                                </span>
                                        <span class="m-topbar__username m--hide">Nick</span>
                                    </a>
                                            <div class="m-dropdown__wrapper">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__header m--align-center">
                                                        <div class="m-card-user m-card-user--skin-light">
                                                            <div class="m-card-user__pic">
                                                                <img src="assets/images/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt="" />
                                                            </div>
                                                            <div class="m-card-user__details">
                                                                <span class="m-card-user__name m--font-weight-500">Mark Andre</span>
                                                                <a href="" class="m-card-user__email m--font-weight-300 m-link">mark.andre@gmail.com</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav m-nav--skin-light">
                                                                <li class="m-nav__section m--hide">
                                                                    <span class="m-nav__section-text">Section</span>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">
                                                                            <span class="m-nav__link-wrap">
                                                                                <span class="m-nav__link-text">My Profile</span>
                                                                                <span class="m-nav__link-badge">
                                                                                    <span class="m-badge m-badge--success">2</span>
                                                                                </span>
                                                                            </span>
                                                                        </span>
                                                            </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                                <span class="m-nav__link-text">Activity</span>
                                                            </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                                <span class="m-nav__link-text">Messages</span>
                                                            </a>
                                                                </li>
                                                                <li class="m-nav__separator m-nav__separator--fit">
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                                <span class="m-nav__link-text">FAQ</span>
                                                            </a>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="profile.html" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                <span class="m-nav__link-text">Support</span>
                                                            </a>
                                                                </li>
                                                                <li class="m-nav__separator m-nav__separator--fit">
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="snippets/pages/user/login-1.html" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="m_quick_sidebar_toggle" class="m-nav__item">
                                            <a href="#" class="m-nav__link m-dropdown__toggle">
                                                <span class="m-nav__link-icon m-nav__link-icon-alt">
                                                    <span class="m-nav__link-icon-wrapper">
                                                        <i class="flaticon-grid-menu"></i>
                                                    </span>
                                                </span>
                                    </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- END: Topbar -->
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: Header -->
            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                <!-- BEGIN: Left Aside -->
                <button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
                    <i class="la la-close"></i>
                </button>
                <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-light ">
                    <!-- BEGIN: Brand -->
                    <div class="m-brand  m-brand--skin-light ">
                        <a href="/" class="m-brand__logo">
                            <img alt="" src="assets/images/logos/air-mobile.png" />
                        </a>
                    </div>
                    <!-- END: Brand -->
                    <!-- BEGIN: Aside Menu -->
                    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " data-menu-vertical="true" m-menu-scrollable="true" m-menu-dropdown-timeout="500">
                        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--submenu-fullheight" aria-haspopup="false" data-toggle="m-popover" data-placement="top" data-content="Dashboard with realtime information.">
                                <a href="/" class="m-menu__link">
                                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                                    <span class="m-menu__link-text">Applications</span>
                                </a>
                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="false" data-toggle="m-popover" data-placement="top" data-content="Landline and mobile numbers.">
                                <a href="/numbers" class="m-menu__link">
                                    <i class="m-menu__link-icon flaticon-squares-3"></i>
                                    <span class="m-menu__link-text">Numbers</span>
                                </a>
                            </li>
                            <li class="m-menu__item  m-menu__item--submenu active" aria-haspopup="false" data-toggle="m-popover" data-placement="top" data-content="Telecom users.">
                                <a href="/users" class="m-menu__link">
                                    <i class="m-menu__link-icon flaticon-users"></i>
                                    <span class="m-menu__link-text">Users</span>
                                </a>
                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="false" data-toggle="m-popover" data-placement="top" data-content="Telecom groups.">
                                <a href="/groups" class="m-menu__link">
                                    <i class="m-menu__link-icon flaticon-network"></i>
                                    <span class="m-menu__link-text">Groups</span>
                                </a>
                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1" data-toggle="m-popover" data-placement="top" data-content="Extra add-ons.">
                                <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon flaticon-app"></i>
                                    <span class="m-menu__link-text">Applications</span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu ">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" m-menu-link-redirect="1">
                                            <span class="m-menu__link">
                                    <span class="m-menu__link-text">Applications</span>
                                            </span>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="/applications" class="m-menu__link ">
                                                <i class="m-menu__link-icon la flaticon-infinity"></i>
                                                <span class="m-menu__link-text">Auto attendant</span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="/applications" class="m-menu__link ">
                                                <i class="m-menu__link-icon la la-commenting"></i>
                                                <span class="m-menu__link-text">Fax</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-2" aria-haspopup="true" m-menu-submenu-toggle="click">
                                <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon flaticon-settings"></i>
                                    <span class="m-menu__link-text">Settings</span>
                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="m-menu__submenu m-menu__submenu--up">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent m-menu__item--bottom-2" aria-haspopup="true">
                                            <span class="m-menu__link">
                                                <span class="m-menu__link-text">Settings</span>
                                            </span>
                                        </li>
                                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link m-menu__toggle">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">Profile</span>
                                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                                            </a>
                                            <div class="m-menu__submenu ">
                                                <span class="m-menu__arrow"></span>
                                                <ul class="m-menu__subnav">
                                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                        <a href="inner.html" class="m-menu__link ">
                                                            <i class="m-menu__link-icon flaticon-computer"></i>
                                                            <span class="m-menu__link-title">
                                                                <span class="m-menu__link-wrap">
                                                                    <span class="m-menu__link-text">Pending</span>
                                                                    <span class="m-menu__link-badge">
                                                                        <span class="m-badge m-badge--warning">10</span>
                                                                    </span>
                                                                </span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                        <a href="inner.html" class="m-menu__link ">
                                                            <i class="m-menu__link-icon flaticon-signs-2"></i>
                                                            <span class="m-menu__link-title">
                                                                <span class="m-menu__link-wrap">
                                                                    <span class="m-menu__link-text">Urgent</span>
                                                                    <span class="m-menu__link-badge">
                                                                        <span class="m-badge m-badge--danger">6</span>
                                                                    </span>
                                                                </span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                        <a href="inner.html" class="m-menu__link ">
                                                            <i class="m-menu__link-icon flaticon-clipboard"></i>
                                                            <span class="m-menu__link-title">
                                                                <span class="m-menu__link-wrap">
                                                                    <span class="m-menu__link-text">Done</span>
                                                                    <span class="m-menu__link-badge">
                                                                        <span class="m-badge m-badge--success">2</span>
                                                                    </span>
                                                                </span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                        <a href="inner.html" class="m-menu__link ">
                                            <i class="m-menu__link-icon flaticon-multimedia-2"></i>
                                            <span class="m-menu__link-title">
                                                                <span class="m-menu__link-wrap">
                                                                    <span class="m-menu__link-text">Archive</span>
                                                                    <span class="m-menu__link-badge">
                                                                        <span class="m-badge m-badge--info m-badge--wide">245</span>
                                                                    </span>
                                                                </span>
                                                            </span>
                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Accounts</span>
                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Help</span>
                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--line">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Notifications</span>
                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-1" aria-haspopup="true" m-menu-submenu-toggle="click">
                                <a href="javascript:;" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-info"></i>
                    <span class="m-menu__link-text">Help</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                                <div class="m-menu__submenu m-menu__submenu--up">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--parent m-menu__item--bottom-1" aria-haspopup="true">
                                            <span class="m-menu__link">
                                                <span class="m-menu__link-text">Help</span>
                                            </span>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Support</span>
                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Blog</span>
                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Documentation</span>
                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Pricing</span>
                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Terms</span>
                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- END: Aside Menu -->
                </div>
                <div class="m-aside-menu-overlay"></div>
                <!-- END: Left Aside -->
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                    <div class="m-content">
                        <div class="row">
                            <div class="col-xl-3">
                                <!--begin:: Widgets/Users overview-->
                                <div class="m-portlet m-portlet--full-height sidebar_main">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-light" id="m_quicksearch" m-quicksearch-mode="default">
                                                <!--BEGIN: Search Form -->
                                                <form class="m-header-search__form">
                                                    <div class="m-header-search__wrapper">
                                                        <span class="m-header-search__icon-search" id="m_quicksearch_search">
                                                            <i class="flaticon-search"></i>
                                                        </span>
                                                        <span class="m-header-search__input-wrapper">
                                                            <input autocomplete="off" type="text" name="q" class="m-header-search__input" value="" placeholder="Search..." id="m_quicksearch_input">
                                                        </span>
                                                        <span class="m-header-search__icon-close" id="m_quicksearch_close">
                                                            <i class="la la-remove"></i>
                                                        </span>
                                                        <span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
                                                            <i class="la la-remove"></i>
                                                        </span>
                                                    </div>
                                                </form>
                                                <!--END: Search Form -->
                                            </div>
                                            <div class="add_users">
                                                <a href="#">
                                                    <i class="fas fa-plus-circle"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="m_widget4_tab1_content">
                                                <!--begin::User overview-->
                                                <div class="m-widget4">
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Jim Brodeau" style="border-radius: 50%;" extension="661" role="admin" email="Brodeau@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
					                                            Jim Brodeau
					                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
						                                        Extension 661
						                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Mara Fedoronko" style="border-radius: 50%;" extension="418" role="user" email="Fedoronko@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
					                                            Mara Fedoronko
					                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
						                                        Extension 418
						                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Nicoli Danes" style="border-radius: 50%;" extension="215" role="user" email="Danes@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
					                                            Nicoli Danes
					                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
						                                        Extension 215
						                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Sergio Mulrean" style="border-radius: 50%;" extension="650" role="user" email="Mulrean@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
					                                            Sergio Mulrean
					                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
						                                        Extension 650
						                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Lanette Iwanowicz" style="border-radius: 50%;" extension="256" role="user" email="Iwanowicz@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
                                            Lanette Iwanowicz
                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
                                        Extension 256
                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Stesha Mathouse" style="border-radius: 50%;" extension="532" role="admin" email="Mathouse@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
                                            Stesha Mathouse
                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
                                        Extension 532
                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Merrili Dust" style="border-radius: 50%;" extension="698" role="user" email="Dust@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
                                            Merrili Dust
                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
                                        Extension 698
                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Jerrome Minerdo" style="border-radius: 50%;" extension="732" role="user" email="Minerdo@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
                                            Jerrome Minerdo
                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
                                        Extension 732
                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Myer Murricanes" style="border-radius: 50%;" extension="333" role="user" email="Murricanes@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
                                            Myer Murricanes
                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
                                        Extension 333
                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::User-->
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--pic">
                                                            <img class="round avatar" width="50" height="50" avatar="Nevin Battrum" style="border-radius: 50%;" extension="544" role="user" email="Battrum@gmail.com" active="false">
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__title">
                                            Nevin Battrum
                                        </span>
                                                            <br>
                                                            <span class="m-widget4__sub">
                                        Extension 544
                                    </span>
                                                        </div>
                                                    </div>
                                                    <!--end::User-->
                                                </div>
                                                <!--end::User overview-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end:: Widgets/ Users overview-->
                            </div>
                            <div id="detail" class="col-xl-9 custom_page" style="display:none;">
                                <div class="m-portlet">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <span class="m-portlet__head-icon m--hide">
						                            <i class="la la-gear"></i>
						                        </span>
                                                <img id="detail[avatar]" class="round" width="50" height="50" avatar="Marcel Verspuij" style="border-radius: 50%;" active="true">
                                                <h3 id="detail[name]" class="m-portlet__head-text" style="padding-left:10px"></h3>
                                            </div>
                                        </div>

                                        <div class="edit_update">
                                            <div class="dropdown show">
                                                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="edit_update" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-h"></i>
                                              </a>
                                                <div class="dropdown-menu" aria-labelledby="edit_update">
                                                    <a class="dropdown-item" href="#">Edit</a>
                                                    <a class="dropdown-item" href="#">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--begin::Form-->
                                    <div class="m-portlet__body row" style="padding-bottom: 0;">
                                    	<div class="custom_colum_1 col-5">
	                                        <div class="m-form__group row">
	                                            <h6>General</h6>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left;" ;>Status</label>
	                                            <div class="col-6">
	                                                <!-- <label for="example-text-input" class="col-form-label">Active</label> -->
                                                    <div class="form_edit">
                                                        <span class="label">Active </span> <i class="fas fa-pencil-alt icon_custom"></i>
                                                        <div class="form-group">
                                                            <select class="form-control" id="exampleFormControlSelect1">
                                                                <option value="Active">Active</option>
                                                                <option value="Holiday">Holiday</option>
                                                                <option value="Disabled">Disabled</option>
                                                            </select>

                                                            <div class="save_ignore">
                                                                <div class="save">
                                                                    <i class="fas fa-check"></i>
                                                                </div>
                                                                <div class="ignore">
                                                                    <i class="fas fa-times"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left">Extension</label>
	                                            <div class="col-6">
	                                                <label id="detail[extension]" class="col-form-label">215</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left">Role</label>
	                                            <div class="col-6">
	                                                <label id="detail[role]" class="col-form-label">User</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left">Email</label>
	                                            <div class="col-6">
	                                                <label id="detail[email]" class="col-form-label">hjahja@gmail.com</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left">Password</label>
	                                            <div class="col-6">
	                                                <label for="example-text-input" class="col-form-label">Reset password</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row">
	                                            <h6 class="col-6 col-form-label" style="padding-left: 0px;">working hours</h6>
	                                            <div class="col-6">
	                                                <label class="col-form-label"><i class="la  la-check"></i></label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left;" ;>Monday</label>
	                                            <div class="col-3">
	                                                <label for="example-text-input" class="col-form-label">08:00</label>
	                                            </div>
	                                            <div class="col-3">
	                                                <label class="col-form-label">17:00</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left; ">Tuesday</label>
	                                            <div class="col-3">
	                                                <label for="example-text-input" class="col-form-label">08:00</label>
	                                            </div>
	                                            <div class="col-3">
	                                                <label class="col-form-label">17:00</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left">Wednesday</label>
	                                            <div class="col-3">
	                                                <label for="example-text-input" class="col-form-label">08:00</label>
	                                            </div>
	                                            <div class="col-3">
	                                                <label class="col-form-label">17:00</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left; padding-bottom: 0;">Thursday</label>
	                                            <div class="col-3">
	                                                <label for="example-text-input" class="col-form-label">08:00</label>
	                                            </div>
	                                            <div class="col-3">
	                                                <label class="col-form-label">17:00</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left">Friday</label>
	                                            <div class="col-3">
	                                                <label for="example-text-input" class="col-form-label">08:00</label>
	                                            </div>
	                                            <div class="col-3">
	                                                <label class="col-form-label">17:00</label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left">Saturday</label>
	                                            <div class="col-6">
	                                                <label for="example-text-input" class="col-form-label"></label>
	                                            </div>
	                                        </div>
	                                        <div class="m-form__group sub_item row" style="padding-top: 0;">
	                                            <label for="example-text-input" class="col-6 col-form-label" style="text-align: left">Sunday</label>
	                                            <div class="col-6">
	                                                <label for="example-text-input" class="col-form-label"></label>
	                                            </div>
	                                        </div>
                                    	</div>
                                    	<div class="custom_colum_2 col-4">
                                    		<div class="item">
	                                    		<div class="m-form__group row">
		                                            <h6>Phone</h6>
		                                        </div>
		                                        <div class="m-form__group row" style="padding-top: 0;">
		                                            <div class="col-12">
		                                                <p class="label">
		                                                	Welcomes message <i class="fas fa-play-circle"></i>
		                                                </p>
		                                            </div>
		                                            <div class="col-12">
		                                                <p class="label">
		                                                	Music on hold <i class="fas fa-play-circle"></i>
		                                                </p>
		                                            </div>
		                                        </div>
                                    		</div>

                                    		<div class="item">
	                                    		<div class="m-form__group row">
		                                            <h6>Incoming lines</h6>
		                                        </div>
		                                        <div class="m-form__group row" style="padding-top: 0;">
		                                            <div class="col-12">
		                                                <p class="label">+3120770991</p>
		                                            </div>
		                                            <div class="col-12">
		                                                <p class="label">+31670987</p>
		                                            </div>
		                                        </div>
                                    		</div>

                                    		<div class="item">
	                                    		<div class="m-form__group row">
		                                            <h6>Groups</h6>
		                                            <h6 class="label_sale">Sales</h6>
		                                        </div>
                                    		</div>

                                    		<div class="item">
	                                    		<div class="m-form__group row">
		                                            <h6>Outgoing caller number</h6>
		                                        </div>
		                                        <div class="m-form__group row" style="padding-top: 0;">
		                                            <div class="col-12">
		                                                <p class="label">Lat called by number <i class="la  la-check"></i></p>
		                                            </div>
		                                            <div class="col-12">
		                                                <p class="label">Business <span>+3120770991</span></p>
		                                            </div>
		                                        </div>
                                    		</div>

                                    		<div class="item">
	                                    		<div class="m-form__group row">
		                                            <h6>External contacts</h6>
		                                        </div>
		                                        <div class="m-form__group row" style="padding-top: 0;">
		                                            <div class="col-12">
		                                                <p class="label">iClould</p>
		                                            </div>
		                                        </div>
                                    		</div>

                                    		<div class="item">
	                                    		<div class="m-form__group row">
		                                            <h6 class="item_last">Intelligence learning <i class="la  la-check"></i></h6>
		                                        </div>
		                                        <div class="m-form__group row" style="padding-top: 0;">
		                                            <div class="col-12">
		                                                <p class="label"><a href="#">Show current rules</a></p>
		                                            </div>
		                                        </div>
                                    		</div>
                                    		<div class="cLear"></div>
                                    	</div>
                                    	<div class="custom_colum_3 col-3">
                                    		<div class="item">
	                                    		<div class="m-form__group row">
		                                            <h6>Phone</h6>
		                                        </div>
		                                        <div class="m-form__group row" style="padding-top: 0;">
		                                            <div class="col-12">
		                                                <p class="label">Package <span>4g unlimited</span></p>
		                                            </div>
		                                            <div class="col-12">
		                                                <p class="label">Used data <span>1,2 GB</span></p>
		                                            </div>
		                                        </div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <!--end::Form-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- begin::Quick Sidebar -->
            <div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
                <div class="m-quick-sidebar__content m--hide">
                    <span id="m_quick_sidebar_close" class="m-quick-sidebar__close">
                    <i class="la la-close"></i>
                </span>
                    <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">Messages</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">Logs</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
                            <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
                                <div class="m-messenger__messages m-scrollable">
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--in">
                                            <div class="m-messenger__message-pic">
                                                <img src="assets/images/app/media/img//users/user3.jpg" alt="" />
                                            </div>
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-username">
                                                        Megan wrote
                                                    </div>
                                                    <div class="m-messenger__message-text">
                                                        Hi Bob. What time will be the meeting ?
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--out">
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-text">
                                                        Hi Megan. It's at 2.30PM
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--in">
                                            <div class="m-messenger__message-pic">
                                                <img src="assets/images/app/media/img//users/user3.jpg" alt="" />
                                            </div>
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-username">
                                                        Megan wrote
                                                    </div>
                                                    <div class="m-messenger__message-text">
                                                        Will the development team be joining ?
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--out">
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-text">
                                                        Yes sure. I invited them as well
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__datetime">2:30PM</div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--in">
                                            <div class="m-messenger__message-pic">
                                                <img src="assets/images/app/media/img//users/user3.jpg" alt="" />
                                            </div>
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-username">
                                                        Megan wrote
                                                    </div>
                                                    <div class="m-messenger__message-text">
                                                        Noted. For the Coca-Cola Mobile App project as well ?
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--out">
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-text">
                                                        Yes, sure.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--out">
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-text">
                                                        Please also prepare the quotation for the Loop CRM project as well.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__datetime">3:15PM</div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--in">
                                            <div class="m-messenger__message-no-pic m--bg-fill-danger">
                                                <span>M</span>
                                            </div>
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-username">
                                                        Megan wrote
                                                    </div>
                                                    <div class="m-messenger__message-text">
                                                        Noted. I will prepare it.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--out">
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-text">
                                                        Thanks Megan. I will see you later.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-messenger__wrapper">
                                        <div class="m-messenger__message m-messenger__message--in">
                                            <div class="m-messenger__message-pic">
                                                <img src="assets/images/app/media/img//users/user3.jpg" alt="" />
                                            </div>
                                            <div class="m-messenger__message-body">
                                                <div class="m-messenger__message-arrow"></div>
                                                <div class="m-messenger__message-content">
                                                    <div class="m-messenger__message-username">
                                                        Megan wrote
                                                    </div>
                                                    <div class="m-messenger__message-text">
                                                        Sure. See you in the meeting soon.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-messenger__seperator"></div>
                                <div class="m-messenger__form">
                                    <div class="m-messenger__form-controls">
                                        <input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
                                    </div>
                                    <div class="m-messenger__form-tools">
                                        <a href="" class="m-messenger__form-attachment">
                                <i class="la la-paperclip"></i>
                            </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="m_quick_sidebar_tabs_logs" role="tabpanel">
                            <div class="m-list-timeline m-scrollable">
                                <div class="m-list-timeline__group">
                                    <div class="m-list-timeline__heading">
                                        System Logs
                                    </div>
                                    <div class="m-list-timeline__items">
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">12 new users registered
                                    <span class="m-badge m-badge--warning m-badge--wide">important</span>
                                </a>
                                            <span class="m-list-timeline__time">Just now</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">System shutdown</a>
                                            <span class="m-list-timeline__time">11 mins</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                            <a href="" class="m-list-timeline__text">New invoice received</a>
                                            <span class="m-list-timeline__time">20 mins</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                            <a href="" class="m-list-timeline__text">Database overloaded 89%
                                    <span class="m-badge m-badge--success m-badge--wide">resolved</span>
                                </a>
                                            <span class="m-list-timeline__time">1 hr</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">System error</a>
                                            <span class="m-list-timeline__time">2 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">Production server down
                                    <span class="m-badge m-badge--danger m-badge--wide">pending</span>
                                </a>
                                            <span class="m-list-timeline__time">3 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">Production server up</a>
                                            <span class="m-list-timeline__time">5 hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-list-timeline__group">
                                    <div class="m-list-timeline__heading">
                                        Applications Logs
                                    </div>
                                    <div class="m-list-timeline__items">
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">New order received
                                    <span class="m-badge m-badge--info m-badge--wide">urgent</span>
                                </a>
                                            <span class="m-list-timeline__time">7 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">12 new users registered</a>
                                            <span class="m-list-timeline__time">Just now</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">System shutdown</a>
                                            <span class="m-list-timeline__time">11 mins</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                            <a href="" class="m-list-timeline__text">New invoices received</a>
                                            <span class="m-list-timeline__time">20 mins</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                            <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                                            <span class="m-list-timeline__time">1 hr</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">System error
                                    <span class="m-badge m-badge--info m-badge--wide">pending</span>
                                </a>
                                            <span class="m-list-timeline__time">2 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">Production server down</a>
                                            <span class="m-list-timeline__time">3 hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-list-timeline__group">
                                    <div class="m-list-timeline__heading">
                                        Server Logs
                                    </div>
                                    <div class="m-list-timeline__items">
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">Production server up</a>
                                            <span class="m-list-timeline__time">5 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">New order received</a>
                                            <span class="m-list-timeline__time">7 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">12 new users registered</a>
                                            <span class="m-list-timeline__time">Just now</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">System shutdown</a>
                                            <span class="m-list-timeline__time">11 mins</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-danger"></span>
                                            <a href="" class="m-list-timeline__text">New invoice received</a>
                                            <span class="m-list-timeline__time">20 mins</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-warning"></span>
                                            <a href="" class="m-list-timeline__text">Database overloaded 89%</a>
                                            <span class="m-list-timeline__time">1 hr</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">System error</a>
                                            <span class="m-list-timeline__time">2 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">Production server down</a>
                                            <span class="m-list-timeline__time">3 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>
                                            <a href="" class="m-list-timeline__text">Production server up</a>
                                            <span class="m-list-timeline__time">5 hrs</span>
                                        </div>
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--state-info"></span>
                                            <a href="" class="m-list-timeline__text">New order received</a>
                                            <span class="m-list-timeline__time">1117 hrs</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end::Quick Sidebar -->
<?php
	require 'footer.php';
?>