<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>Air Mobile | Users overview</title>
    <meta name="description" content="Blank inner page examples">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });

    <?php 
        require('init.php'); 
    ?>

    </script>
    <!--end::Web font -->
    <!--begin::Page Vendors Styles -->
    <link href="assets/styles/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--begin::Base Styles -->
    <link href="assets/styles/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="assets/styles/demo/demo7/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="assets/demo/demo11/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <!--end::Base Styles -->
    <link rel="shortcut icon" type="image/png" href="assets/images/favicons/air-mobile.png" />
    <script>
    <!-- Google Analytics code -->
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('dist/app.css'); ?>">
    <script type="text/javascript" src="<?php echo site_url('dist/app.js'); ?>"></script>
</head>
<!-- end::Head -->
<!-- begin::Body -->
<!-- begin::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">