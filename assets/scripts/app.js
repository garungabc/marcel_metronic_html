// Uncomment the next line if you want to use bootstrap, don't forget uncomment jQuery defination in webpack.common.js line 93
// import 'bootstrap';

(function($) {
    console.log('My extension is ready :)');
})(jQuery)

// (function($) {
$(document).ready(function() {
    $('.icon_custom').click(function() {
        $(this).parent('.form_edit').find('span.label').css({ 'display': 'none' });
        $(this).css({ 'display': 'none' });
        $(this).parent('.form_edit').find('.form-group').css({ 'display': 'block' });
        $(this).parent('.form_edit').find('.form-group .save_ignore i').css({ 'display': 'block' });

    })
    $('.save').click(function() {
        var val_elemet = $('#exampleFormControlSelect1').val();

        $('.form_edit').find('span.label').html(val_elemet).css({ 'display': 'inline-block' });
        $('.icon_custom').removeAttr("style");
        $('.form-group').hide();
    });

    $('.ignore').click(function() {
        var val_elemet_ignore = $('#exampleFormControlSelect1').val();

        $('.form_edit').find('span.label').html(val_elemet_ignore).css({ 'display': 'inline-block' });
        $('.icon_custom').removeAttr("style");
        $('.form-group').hide();
    });
});
// })(jQuery)