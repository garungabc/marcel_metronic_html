/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/assets/dist";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/scripts/app.js":
/*!*******************************!*\
  !*** ./assets/scripts/app.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// Uncomment the next line if you want to use bootstrap, don't forget uncomment jQuery defination in webpack.common.js line 93
// import 'bootstrap';

(function ($) {
    console.log('My extension is ready :)');
})(jQuery);

// (function($) {
$(document).ready(function () {
    $('.icon_custom').click(function () {
        $(this).parent('.form_edit').find('span.label').css({ 'display': 'none' });
        $(this).css({ 'display': 'none' });
        $(this).parent('.form_edit').find('.form-group').css({ 'display': 'block' });
        $(this).parent('.form_edit').find('.form-group .save_ignore i').css({ 'display': 'block' });
    });
    $('.save').click(function () {
        var val_elemet = $('#exampleFormControlSelect1').val();

        $('.form_edit').find('span.label').html(val_elemet).css({ 'display': 'inline-block' });
        $('.icon_custom').removeAttr("style");
        $('.form-group').hide();
    });

    $('.ignore').click(function () {
        var val_elemet_ignore = $('#exampleFormControlSelect1').val();

        $('.form_edit').find('span.label').html(val_elemet_ignore).css({ 'display': 'inline-block' });
        $('.icon_custom').removeAttr("style");
        $('.form-group').hide();
    });
});
// })(jQuery)

/***/ }),

/***/ "./assets/styles/app.scss":
/*!********************************!*\
  !*** ./assets/styles/app.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!**************************************************************!*\
  !*** multi ./assets/scripts/app.js ./assets/styles/app.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./assets/scripts/app.js */"./assets/scripts/app.js");
module.exports = __webpack_require__(/*! ./assets/styles/app.scss */"./assets/styles/app.scss");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3NjcmlwdHMvYXBwLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9zdHlsZXMvYXBwLnNjc3MiXSwibmFtZXMiOlsiJCIsImNvbnNvbGUiLCJsb2ciLCJqUXVlcnkiLCJkb2N1bWVudCIsInJlYWR5IiwiY2xpY2siLCJwYXJlbnQiLCJmaW5kIiwiY3NzIiwidmFsX2VsZW1ldCIsInZhbCIsImh0bWwiLCJyZW1vdmVBdHRyIiwiaGlkZSIsInZhbF9lbGVtZXRfaWdub3JlIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFDQTs7QUFFQSxDQUFDLFVBQVNBLENBQVQsRUFBWTtBQUNUQyxZQUFRQyxHQUFSLENBQVksMEJBQVo7QUFDSCxDQUZELEVBRUdDLE1BRkg7O0FBSUE7QUFDQUgsRUFBRUksUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVc7QUFDekJMLE1BQUUsY0FBRixFQUFrQk0sS0FBbEIsQ0FBd0IsWUFBVztBQUMvQk4sVUFBRSxJQUFGLEVBQVFPLE1BQVIsQ0FBZSxZQUFmLEVBQTZCQyxJQUE3QixDQUFrQyxZQUFsQyxFQUFnREMsR0FBaEQsQ0FBb0QsRUFBRSxXQUFXLE1BQWIsRUFBcEQ7QUFDQVQsVUFBRSxJQUFGLEVBQVFTLEdBQVIsQ0FBWSxFQUFFLFdBQVcsTUFBYixFQUFaO0FBQ0FULFVBQUUsSUFBRixFQUFRTyxNQUFSLENBQWUsWUFBZixFQUE2QkMsSUFBN0IsQ0FBa0MsYUFBbEMsRUFBaURDLEdBQWpELENBQXFELEVBQUUsV0FBVyxPQUFiLEVBQXJEO0FBQ0FULFVBQUUsSUFBRixFQUFRTyxNQUFSLENBQWUsWUFBZixFQUE2QkMsSUFBN0IsQ0FBa0MsNEJBQWxDLEVBQWdFQyxHQUFoRSxDQUFvRSxFQUFFLFdBQVcsT0FBYixFQUFwRTtBQUVILEtBTkQ7QUFPQVQsTUFBRSxPQUFGLEVBQVdNLEtBQVgsQ0FBaUIsWUFBVztBQUN4QixZQUFJSSxhQUFhVixFQUFFLDRCQUFGLEVBQWdDVyxHQUFoQyxFQUFqQjs7QUFFQVgsVUFBRSxZQUFGLEVBQWdCUSxJQUFoQixDQUFxQixZQUFyQixFQUFtQ0ksSUFBbkMsQ0FBd0NGLFVBQXhDLEVBQW9ERCxHQUFwRCxDQUF3RCxFQUFFLFdBQVcsY0FBYixFQUF4RDtBQUNBVCxVQUFFLGNBQUYsRUFBa0JhLFVBQWxCLENBQTZCLE9BQTdCO0FBQ0FiLFVBQUUsYUFBRixFQUFpQmMsSUFBakI7QUFDSCxLQU5EOztBQVFBZCxNQUFFLFNBQUYsRUFBYU0sS0FBYixDQUFtQixZQUFXO0FBQzFCLFlBQUlTLG9CQUFvQmYsRUFBRSw0QkFBRixFQUFnQ1csR0FBaEMsRUFBeEI7O0FBRUFYLFVBQUUsWUFBRixFQUFnQlEsSUFBaEIsQ0FBcUIsWUFBckIsRUFBbUNJLElBQW5DLENBQXdDRyxpQkFBeEMsRUFBMkROLEdBQTNELENBQStELEVBQUUsV0FBVyxjQUFiLEVBQS9EO0FBQ0FULFVBQUUsY0FBRixFQUFrQmEsVUFBbEIsQ0FBNkIsT0FBN0I7QUFDQWIsVUFBRSxhQUFGLEVBQWlCYyxJQUFqQjtBQUNILEtBTkQ7QUFPSCxDQXZCRDtBQXdCQSxhOzs7Ozs7Ozs7OztBQ2hDQSx5QyIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIi9hc3NldHMvZGlzdFwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG4iLCIvLyBVbmNvbW1lbnQgdGhlIG5leHQgbGluZSBpZiB5b3Ugd2FudCB0byB1c2UgYm9vdHN0cmFwLCBkb24ndCBmb3JnZXQgdW5jb21tZW50IGpRdWVyeSBkZWZpbmF0aW9uIGluIHdlYnBhY2suY29tbW9uLmpzIGxpbmUgOTNcclxuLy8gaW1wb3J0ICdib290c3RyYXAnO1xyXG5cclxuKGZ1bmN0aW9uKCQpIHtcclxuICAgIGNvbnNvbGUubG9nKCdNeSBleHRlbnNpb24gaXMgcmVhZHkgOiknKTtcclxufSkoalF1ZXJ5KVxyXG5cclxuLy8gKGZ1bmN0aW9uKCQpIHtcclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcbiAgICAkKCcuaWNvbl9jdXN0b20nKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAkKHRoaXMpLnBhcmVudCgnLmZvcm1fZWRpdCcpLmZpbmQoJ3NwYW4ubGFiZWwnKS5jc3MoeyAnZGlzcGxheSc6ICdub25lJyB9KTtcclxuICAgICAgICAkKHRoaXMpLmNzcyh7ICdkaXNwbGF5JzogJ25vbmUnIH0pO1xyXG4gICAgICAgICQodGhpcykucGFyZW50KCcuZm9ybV9lZGl0JykuZmluZCgnLmZvcm0tZ3JvdXAnKS5jc3MoeyAnZGlzcGxheSc6ICdibG9jaycgfSk7XHJcbiAgICAgICAgJCh0aGlzKS5wYXJlbnQoJy5mb3JtX2VkaXQnKS5maW5kKCcuZm9ybS1ncm91cCAuc2F2ZV9pZ25vcmUgaScpLmNzcyh7ICdkaXNwbGF5JzogJ2Jsb2NrJyB9KTtcclxuXHJcbiAgICB9KVxyXG4gICAgJCgnLnNhdmUnKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgdmFsX2VsZW1ldCA9ICQoJyNleGFtcGxlRm9ybUNvbnRyb2xTZWxlY3QxJykudmFsKCk7XHJcblxyXG4gICAgICAgICQoJy5mb3JtX2VkaXQnKS5maW5kKCdzcGFuLmxhYmVsJykuaHRtbCh2YWxfZWxlbWV0KS5jc3MoeyAnZGlzcGxheSc6ICdpbmxpbmUtYmxvY2snIH0pO1xyXG4gICAgICAgICQoJy5pY29uX2N1c3RvbScpLnJlbW92ZUF0dHIoXCJzdHlsZVwiKTtcclxuICAgICAgICAkKCcuZm9ybS1ncm91cCcpLmhpZGUoKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoJy5pZ25vcmUnKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgdmFsX2VsZW1ldF9pZ25vcmUgPSAkKCcjZXhhbXBsZUZvcm1Db250cm9sU2VsZWN0MScpLnZhbCgpO1xyXG5cclxuICAgICAgICAkKCcuZm9ybV9lZGl0JykuZmluZCgnc3Bhbi5sYWJlbCcpLmh0bWwodmFsX2VsZW1ldF9pZ25vcmUpLmNzcyh7ICdkaXNwbGF5JzogJ2lubGluZS1ibG9jaycgfSk7XHJcbiAgICAgICAgJCgnLmljb25fY3VzdG9tJykucmVtb3ZlQXR0cihcInN0eWxlXCIpO1xyXG4gICAgICAgICQoJy5mb3JtLWdyb3VwJykuaGlkZSgpO1xyXG4gICAgfSk7XHJcbn0pO1xyXG4vLyB9KShqUXVlcnkpIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9