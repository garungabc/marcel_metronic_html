<?php 

function site_url($uri = '') {
    $site_url = 'http://metronic.trada-it.com';
    if($uri != '') {
        $site_url .= '/'. $uri;
    }
    return $site_url;
}
 
function asset($assets)
{
    return site_url() . '/assets/dist/' . $assets;
}

function asset2($assets)
{
    return site_url() . '/assets/' . $assets;
}

